{
  lib,
  stdenvNoCC,
  fetchFromGitLab,
}:
stdenvNoCC.mkDerivation rec {
  pname = "Vulpix";
  version = "1.0.0";

  src = fetchFromGitLab {
    owner = "bulkiestpizza";
    repo = "Vulpix-Cursors";
    rev = "v${version}";
    sha256 = "sha256-PI7381xf/GctQTnfcE0W3M3z2kqbX4VexMf17C61hT8=";
  };

  dontBuild = true;

  installPhase = ''
    mkdir -p $out/share/icons
    mv theme/Vulpix $out/share/icons
  '';

}
