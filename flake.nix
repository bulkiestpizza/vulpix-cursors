{
  description = "vulpix-cursors";


  outputs = { self, nixpkgs }: {
  	packages.x86_64-linux.vulpix =
	let 
	pkgs = import nixpkgs { system = "x86_64-linux"; };
	in 
	pkgs.stdenv.mkDerivation {
		name = "Vulpix";
		src = self;
		buildPhase = ''
			mkdir -p $out/share/icons
			mv theme/Vulpix $out/share/icons
		'';
	};

  };
}
